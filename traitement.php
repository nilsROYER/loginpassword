<?php
    // je crée une variable $test qui nous est utile plus tard pour contrôler les actions.
    $test = false; 
    // je sécurise la variable $_POST['pseudo'] envoyée par l'utilisateur.
    if(isset($_POST['pseudo'])){
        $_POST['pseudo'] = htmlspecialchars($_POST['pseudo']);
        $test = true;
    }else{
        $test = false;
    }
    // je sécurise la variable $_POST['password'] envoyée par l'utilisateur.
    if(isset($_POST['password'])){
        if($_POST['password'] == $_POST['password2']){
            $_POST['password'] = htmlspecialchars($_POST['password']);
            $pass_hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $test = true;
        }else{
            $test = false;
        }
    }else{
        $test = false;
    }
    // je sécurise la variable $_POST['email'] envoyée par l'utilisateur.
    if(isset($_POST['email'])){
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $test = true;
        }else{
            $test = false;
        }
    }else{
        $test = false;
    }
    
    if($test){ // Première opération : si les variables $_POST sont valides ($test = true) on peut lancer la premiére opération.
        // On se connecte à la base de données.
        $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
        // Requête SQL : compte tout les champs de la table "member" qui ont un pseudo ou un mot de passe ou un email égale au variable $_POST 
        $verif = $bdd->prepare('SELECT COUNT(*) AS x FROM member WHERE pseudo = ? OR mdp = ? OR mail = ?');
        $verif->execute([
            $_POST['pseudo'],
            $pass_hash,
            $_POST['email']
        ]);
        // On vérifie que les données se trouvent ou pas dans la table.
        while($data = $verif->fetch()){

            if($data['x'] == 0){  // Si les données ne sont pas dans la table alors on peut passer au stockage.
                $test = true;
            
		    }else{  // Sinon si notre variable --> $test = false <-- cette variable contrôle les opérations.
                $test = false;
            }

	    }

    }
    if($test){  // Deuxiéme opération : si $test = true alors on insère les données cela signifie que les identifiants n'existent pas et qu'ils sont donc valides.
        // Requête SQL : insère dans la table member un nouveau champ, les données inserées dans son champ sont égales aux variables $_POST [pseudo], [mot de passe], [email]
        $result = $bdd->prepare('INSERT INTO member(pseudo, mdp, mail) VALUE(:pseudo, :mdp, :mail)');
        $result->execute(array(
            'pseudo' => $_POST['pseudo'],
            'mdp' => $pass_hash,
            'mail' => $_POST['email']
        ));
        
        // Aprés l'insertion des données ont détruit les variables car on en n'a plus besoin.
        unset($_POST['pseudo'], $pass_hash, $_POST['email'], $_POST['password'], $_POST['password2']);
        // On crée un cookie utile pour déboguer et pour la suite de la connexion.
        setcookie("valid", true, time()+60);
        // Une fois les identifiants mémorisés, on redirige vers la page de connexion.
        header('location: connexion.php');
        
    }else{  // Sinon si $test = false cela signifie que les données sont déjà dans la table et donc que l'utilisateur doit changer d'identifiant.
        // On détruit les variables car on en n'a plus besoin.
        unset($_POST['pseudo'], $pass_hash, $_POST['email'], $_POST['password'], $_POST['password2']);
        // On crée un cookie utile pour le débog et la suite du programme.
        setcookie("no_valid", true, time()+60);
        // On redirige l'utilisateur vers la page d'inscription, grâce au cookie un message lui indique que ses identifiants sont déjà pris.
        header('location: index.php');
    }

    



?>