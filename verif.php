<?php 
    $test = false;
    if(isset($_POST['pseudo'])){
        $_POST['pseudo'] = htmlspecialchars($_POST['pseudo']);
        $test = true;
    }else{
        $test = false;
    }

    if(isset($_POST['email'])){
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $test = true;
        }else{
            $test = false;
        }
    }else{
        $test = false;
    }

     if(isset($_POST['password'])){
        $_POST['password'] = htmlspecialchars($_POST['password']);

        $test = true;
       
    }else{
        $test = false;
    }

    // Si les variables $_POST sont valides alors $test = true on peut lancer la premiére opération.
    if($test){ // Premiére opération : si $test = true alors on traite les données.
        // On se connecte à la base de données.
        $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
        // Requête SQL : renvoie en champ x qui contient le nombre de champs qui posséde un pseudo et un mail égaux aux variable $_POST.
        $verif = $bdd->prepare('SELECT COUNT(*) AS x FROM member WHERE pseudo = ? AND mail = ?');
        $verif->execute(array(
            $_POST['pseudo'],
            $_POST['email']
        ));

        while($data =  $verif->fetch()){

            if($data['x'] == 1){ // Si $data['x'] = 1 alors cela veut dire que les données de l'utilisateur font bien partie du même champ sinon n'importe quel utilisateur pourait se connecter avec n'importe quel mot de passe présent dans la table.
                $test = true; 
            }else{ // Sinon cela signifie que les données utilisées sont incorectes.
                $test = false;
            }
        }
    }

    if($test){ // Deuxiéme opération : si $test = true alors les données pseudo et mail appartiennent bien à la même persone. On peut donc maintenant tester le mot de passe.
        // Requéte SQL : sélectione tout dans la table "member" qui ont des champs dont les collones speudo et mail égales aux variables $_POST ['pseudo'], ['email'].
        $verif = $bdd->prepare('SELECT * FROM member WHERE pseudo = ? AND mail = ?');
        $verif->execute(array(
            $_POST['pseudo'],
            $_POST['email']
        ));
        // On vérifie que les données se trouvent ou pas dans la table.
        while($data = $verif->fetch()){
            // Si les données sont dans le champ alors on peut établir la connexion, sinon il y a une ereur.
            if($data['pseudo'] == $_POST['pseudo']){  
                $test = true;
		    }else{
                $test = false;
            }

            if($data['mail'] == $_POST['email']){  
                $test = true;
		    }else{
                $test = false;
            }

            if(password_verify($_POST['password'], $data['mdp'])){ // Ici on vérifie que le mot de passe rentré par l'utilisateur correspond au mot de passe chiffré dans la table.
                $test = true;
		    }else{
                $test = false;
            }

        }
    }

    if($test){ // Troisième opération : si $test est toujours = true alors la connexion est établie.
        // On détruit les variables $_POST car on en n'a plus besoin.
        unset($_POST['pseudo'], $_POST['password'], $_POST['email'], $pass_hash);
        setcookie('valid_connec', true, time()+60);
        // On dirige l'utilisateur vers l'espace membre.
        header('location: espacePerso.php');
        
    }else{ // Sinon si $test est devenu false alors cela signifie qu'il y a une erreur dans les données, que le compte n'éxiste pas ou que l'utilisateur a fait des fautes de frappe.
        // On détruit les variables car on en n'a plus besoin.
        unset($_POST['pseudo'], $_POST['password'], $_POST['email'], $pass_hash);
        // On crée un cookie utile pour expliquer à l'utilisateur qu'il y a eu une erreur.
        setcookie('invalid_connec', true, time()+60);
        // On redirige l'utilisateur vers la page de connexion, grâce au cookie un message en plus apparait sur la page pour expliquer à l'utilisateur qu'il y a eu une erreur.
        header('location: connexion.php');
    }
    
?>