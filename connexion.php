<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css?family=Khula&display=swap" rel="stylesheet">

        <title>Connexion</title>
    </head>

    <body>
        <h1>connexion</h1>
        <?php if(isset($_COOKIE['valid']) && !empty($_COOKIE['valid'])):?>
        <p><strong>votre compte a été créer</strong></p>
        <?php endif; ?>
        <?php if(isset($_COOKIE['invalid_connec']) && !empty($_COOKIE['invalid_connec'])):?>
        <p><strong>votre identifiant ou votre mot de passe est incorect</strong></p>
        <?php endif; ?>

        <form action="verif.php" method="post">
            <label for="pseudo">
                <ion-icon name="person-outline"></ion-icon>Login (<em>pseudo</em>)
            </label><br>
            <input type="text" name="pseudo" id="pseudo"><br>
            <label for="password">
                <ion-icon name="lock-closed-outline"></ion-icon>Password
            </label><br>
            <input type="text" name="password" id="password"><br>
            <label for="email">
                <ion-icon name="mail-outline"></ion-icon>E-mail
            </label><br>
            <input type="email" name="email" id="email"><br>
            <button type="submit">connexion</button>
        </form>

        <p>tu n'a pas de compte ?<br><a href="index.php">inscrit-toi !</a></p>
        <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    </body>

</html>